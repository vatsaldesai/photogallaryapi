﻿using PhotoGallary_BAL;
using PhotoGallary_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PhotoGallaryAPI.Controllers
{
    [RoutePrefix("Api/Task")]
    public class TaskController : ApiController
    {
        Task_BAL taskEntities = new Task_BAL();

        [HttpGet]
        [Route(Name ="Tasks")]
        public HttpResponseMessage GetAllTask()
        {
            try
            {
                var tasks = taskEntities.GetAllTask();
                if (tasks != null)
                    return Request.CreateResponse(HttpStatusCode.OK, tasks);
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No Tasks are available");
            }
            catch (Exception ex)
            {

                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("UserAllTask")]
        public HttpResponseMessage GetAllTaskOfUser(int userId)
        {
            try
            {
                if (userId > 0)
                {
                    var tasks = taskEntities.GetAllTaskOfUser(userId);
                    if (tasks != null)
                        return Request.CreateResponse(HttpStatusCode.OK, tasks);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "No Tasks are available");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "No Tasks are available");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("Task")]
        public HttpResponseMessage GetTaskById(int taskId)
        {
            try
            {
                if (taskId > 0)
                {
                    var task = taskEntities.GetTaskByID(taskId);
                    if (task != null)
                        return Request.CreateResponse(HttpStatusCode.OK, task);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Task is not available");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Task is not available");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("CreateTask")]
        public HttpResponseMessage CreateTask(Task newTask, int userId)
        {
            try
            {
                if (newTask!=null && userId > 0)
                {
                    var task = taskEntities.CreateTask(newTask);
                    if (task > 0)
                    {
                        var taskMapID=taskEntities.MapTaskToUser(task, userId);
                        if (taskMapID > 0)
                            return Request.CreateResponse(HttpStatusCode.OK, task);
                        else
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide proper details");
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide proper details");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide proper details");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("EditTask")]
        public HttpResponseMessage EditTask(Task editTask)
        {
            try
            {
                if (editTask != null && editTask.TaskID > 0)
                {
                    var task = taskEntities.EditTask(editTask,editTask.TaskID);
                    if (task != null)
                        return Request.CreateResponse(HttpStatusCode.OK, task);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide proper details");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please provide proper details");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("DeleteTask")]
        public HttpResponseMessage DeleteTask(int taskId)
        {
            try
            {
                if (taskId > 0)
                {
                    taskEntities.DeleteTask(taskId);
                    return Request.CreateResponse(HttpStatusCode.OK, "Task Deleted Succesfully");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Task ias not available");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("TasksByStatus")]
        public HttpResponseMessage GetAllTasksByStatus(int statusId)
        {
            try
            {
                if (statusId > 0)
                {
                    var tasks = taskEntities.GetAllTaskByStatus(statusId);
                    if (tasks != null)
                        return Request.CreateResponse(HttpStatusCode.OK, tasks);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "There are no " + Enum.GetName(typeof(TaskStatus), statusId) + "tasks available.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Internal server error.");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("UserTasksByStatus")]
        public HttpResponseMessage GetAllTaskByStatusAndUserId(int statusId, int userId)
        {
            try
            {
                if (statusId > 0 && userId>0)
                {
                    var tasks = taskEntities.GetAllTaskByStatusAndUserId(statusId,userId);
                    if (tasks != null)
                        return Request.CreateResponse(HttpStatusCode.OK, tasks);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "There are no " + Enum.GetName(typeof(TaskStatus), statusId) + "tasks available for this user.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Internal server error.");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("ChangeStatus")]
        public HttpResponseMessage ChangeStatusOfTask(int statusId, int taskId)
        {
            try
            {
                if (statusId > 0 && taskId > 0)
                {
                    var task = taskEntities.ChangeStatusOfTask(taskId, statusId);
                    if (task != null)
                        return Request.CreateResponse(HttpStatusCode.OK, task);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "There are no " + Enum.GetName(typeof(TaskStatus), statusId) + "tasks available for this user.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Internal server error.");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
