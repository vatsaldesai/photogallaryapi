﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PhotoGallary_DAL;
using PhotoGallary_BAL;
using System.Web.Http.Cors;

namespace PhotoGallaryAPI.Controllers
{   
    [RoutePrefix("Api/Account")]
    public class AccountController : ApiController
    {
        User_BAL userEntities = new User_BAL();

        [HttpPost]
        //[Route(Name ="Login")]
        public HttpResponseMessage Login(string email, string password)
        {
            try
            {
                if (email != null && password != null)
                {
                    var user = userEntities.Login(email, password);
                    if(user != null)
                        return Request.CreateResponse(HttpStatusCode.OK, user);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Please enter valid details.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please enter valid details.");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("GetUsers")]
        public HttpResponseMessage GetAllUser()
        {
            try
            {
                var userList = userEntities.GetAllUser();
                return Request.CreateResponse(HttpStatusCode.OK, userList);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpGet]
        [Route("GetUser")]
        public HttpResponseMessage GetUserById(int Id)
        {
            try
            {
                var user = userEntities.GetUserById(Id);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        [Route("AddUser")]
        public HttpResponseMessage AddUser([FromBody] User newUser)
        {
            try
            {
                var userId = userEntities.AddNewUser(newUser);
                if (userId > 0)
                    return Request.CreateResponse(HttpStatusCode.Created, "User Created Succesfully.");
                else
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "User is Already Exist");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        [Route("EditUser")]
        public HttpResponseMessage EditUser(User User, int id = 0)
        {
            try
            {
                var user = userEntities.UpdateUser(User, id);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

        [HttpDelete]
        [Route("DeleteUser")]
        public HttpResponseMessage DeleteUser(int userId)
        {
            try
            {
                userEntities.DeleteUser(userId);
                return Request.CreateResponse(HttpStatusCode.OK, "User Deleted Sucessfully");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

    }
}
