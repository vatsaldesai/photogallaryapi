﻿using PhotoGallary_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoGallary_BAL
{
    public class Task_BAL
    {
        PhotoGallaryDBEntities entites = new PhotoGallaryDBEntities();

        public IEnumerable<Task> GetAllTask()
        {
            try
            {
                return entites.Tasks.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Task> GetAllTaskOfUser(int userId)
        {
            try
            {
                var allTask = (from m in entites.Tasks
                               join n in entites.Task_User_Map on m.TaskID equals n.TaskID
                               where n.UserID == userId
                               select m).ToList();
                
                return allTask;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task GetTaskByID(int taskId)
        {
            try
            {
                return entites.Tasks.Where(x => x.TaskID == taskId).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CreateTask(Task newTask)
        {
            try
            {
                if (newTask != null)
                {
                    newTask.TaskStatus = 1;
                    entites.Tasks.Add(newTask);
                    entites.SaveChanges();
                    return newTask.TaskID;
                }
                else
                    return newTask.TaskID;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int MapTaskToUser(int taskID,int userId)
        {
            try
            {
                Task_User_Map mapTask = new Task_User_Map();
                mapTask.TaskID = taskID;
                mapTask.UserID = userId;
                entites.Task_User_Map.Add(mapTask);
                entites.SaveChanges();
                return mapTask.ID;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task EditTask(Task editTask,int taskId)
        {
            Task objTask = new Task();
            try
            {
                if(editTask!=null && taskId > 0)
                {
                    objTask = entites.Tasks.Where(x => x.TaskID == taskId).FirstOrDefault();
                    if (objTask != null)
                    {
                        objTask.TaskTitle = editTask.TaskTitle;
                        objTask.TaskDescription = editTask.TaskDescription;
                        objTask.TaskDate = editTask.TaskDate;
                        objTask.TaskHours = editTask.TaskHours;
                        objTask.TaskStatus = editTask.TaskStatus;
                        entites.SaveChanges();
                        return objTask;
                    }
                    else
                    {
                        return objTask;
                    }
                }
                else
                {
                    return objTask;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteTask(int taskId)
        {
            try
            {
                var objTask = entites.Tasks.SingleOrDefault(m => m.TaskID == taskId);
                entites.Tasks.Remove(objTask);
                entites.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Task> GetAllTaskByStatus(int taskStatus)
        {
            try
            {
                return entites.Tasks.Where(x => x.TaskStatus == taskStatus).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<Task> GetAllTaskByStatusAndUserId(int taskStatus, int userId)
        {
            try
            {
                var allTask = (from m in entites.Tasks
                               join n in entites.Task_User_Map on m.TaskID equals n.TaskID
                               where n.UserID == userId && m.TaskStatus == taskStatus
                               select m).ToList();

                return allTask;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task ChangeStatusOfTask(int taskId,int statusId)
        {
            try
            {
                Task objTask = new Task();
                if (taskId>0&&statusId >0)
                {
                    objTask = entites.Tasks.Where(x => x.TaskID == taskId).FirstOrDefault();
                    if (objTask != null)
                    {
                        objTask.TaskStatus = statusId;
                        entites.SaveChanges();
                        
                    }                    
                }
                return objTask;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
