﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoGallary_BAL
{
    public enum TaskStatus
    {
        Active=1,
        InProgress=2,
        Closed=3
    }
}
