﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhotoGallary_DAL;
using System.Data.Entity.Core.Objects;

namespace PhotoGallary_BAL
{
    public class User_BAL
    {
        PhotoGallaryDBEntities entites = new PhotoGallaryDBEntities();

        public IEnumerable<User> GetAllUser()
        {
            try
            {
                return entites.Users.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User GetUserById(int Id)
        {
            try
            {
                return entites.Users.SingleOrDefault(m => m.UserId == Id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int AddNewUser(User newUser)
        {
            try
            {
                ObjectParameter outputId = new ObjectParameter("ID", typeof(Int32));
                ObjectParameter outputMessage = new ObjectParameter("Message", typeof(String));

                entites.USP_Insert_Update_User(
                    newUser.UserId,
                    newUser.FirstName,
                    newUser.LastName,
                    newUser.Password,
                    DateTime.UtcNow,
                    newUser.UserAboutUs,
                    Guid.NewGuid().ToString(),
                    newUser.Email,
                    newUser.UserPhotosPath,
                    outputId,
                    outputMessage);

                return (int)outputId.Value;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public int UpdateUser(User user, int userId)
        {
            try
            {
                ObjectParameter outputId = new ObjectParameter("ID", typeof(Int32));
                ObjectParameter outputMessage = new ObjectParameter("Message", typeof(String));

                entites.USP_Insert_Update_User(
                    userId,
                    user.FirstName,
                    user.LastName,
                    user.Password,
                    DateTime.UtcNow,
                    user.UserAboutUs,
                    Guid.NewGuid().ToString(),
                    user.Email,
                    user.UserPhotosPath,
                    outputId,
                    outputMessage);

                return (int)outputId.Value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteUser(int userId)
        {
            try
            {
                entites.USP_DeleteUser(userId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public User Login(string email,string password)
        {
            try
            {
               return entites.Users.SingleOrDefault(m => m.Email == email && m.Password == password);
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
