//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PhotoGallary_DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public System.DateTime CreatedUTCTime { get; set; }
        public string UserAboutUs { get; set; }
        public string UserGUID { get; set; }
        public string Email { get; set; }
        public string UserPhotosPath { get; set; }
    }
}
